webpackJsonp([3],{

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnboardPageModule", function() { return OnboardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__onboard__ = __webpack_require__(282);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var OnboardPageModule = /** @class */ (function () {
    function OnboardPageModule() {
    }
    OnboardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__onboard__["a" /* OnboardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__onboard__["a" /* OnboardPage */])
            ],
        })
    ], OnboardPageModule);
    return OnboardPageModule;
}());

//# sourceMappingURL=onboard.module.js.map

/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_onboarding_onboarding__ = __webpack_require__(195);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OnboardPage = /** @class */ (function () {
    function OnboardPage(onboardingProvider, navCtrl) {
        this.onboardingProvider = onboardingProvider;
        this.navCtrl = navCtrl;
        this.slides = [
            {
                description: 'Lorem ipsum dolor sit amet',
                image: 'assets/img/logo.png',
                button: 'Próximo',
                anim: ''
            },
            {
                description: 'Lorem ipsum dolor sit amet',
                image: 'assets/img/logo.png',
                button: 'Próximo',
                anim: ''
            },
            {
                description: 'Lorem ipsum dolor sit amet',
                image: 'assets/img/logo.png',
                button: 'Próximo',
                anim: ''
            }
        ];
    }
    OnboardPage.prototype.proximo = function () {
        if (this.slider.isEnd())
            return this.navCtrl.push('login');
        this.slider.slideNext();
    };
    OnboardPage.prototype.onboardingFinalizado = function () {
        var _this = this;
        this.onboardingProvider.visualizado(true)
            .subscribe(function () { return _this.navCtrl.setRoot('acessar'); });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Slides */])
    ], OnboardPage.prototype, "slider", void 0);
    OnboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-onboard',template:/*ion-inline-start:"/Users/luizinho/Desktop/marechal/src/pages/onboard/onboard.html"*/'<ion-header no-border>\n  <ion-navbar>\n  </ion-navbar>\n</ion-header>\n\n<ion-content no-bounce>\n  <!-- <div class="img-scroll move"></div> -->\n  <ion-slides #tutorial pager>\n    <ion-slide *ngFor="let slide of slides;">\n      <div>\n        <div [ngClass]="slide.anim"></div>\n      </div>\n      <img [src]="slide.image" class="slide-image"/>\n      <p [innerHTML]="slide.description"></p>\n      <ion-buttons>\n        <button ion-button round icon-right *ngIf="!slider.isEnd()" (click)="proximo()">\n          Próximo\n        </button>\n        <button class="iniciar-btn" ion-button round icon-right *ngIf="slider.isEnd()" (click)="proximo()">\n          Começar\n        </button>\n      </ion-buttons>\n    </ion-slide>\n  </ion-slides>\n  <!-- <div class="img-scroll move rodape"></div> -->\n  <!-- <button class="skip-btn" clear ion-button small [hidden]="slider.isEnd()" (click)="fim.emit()">\n    Pular\n  </button> -->\n</ion-content>'/*ion-inline-end:"/Users/luizinho/Desktop/marechal/src/pages/onboard/onboard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_onboarding_onboarding__["a" /* OnboardingProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], OnboardPage);
    return OnboardPage;
}());

//# sourceMappingURL=onboard.js.map

/***/ })

});
//# sourceMappingURL=3.js.map