import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { OnboardingProvider } from '../../providers/onboarding/onboarding';
import { Slides } from 'ionic-angular';

@IonicPage({
  name: 'onboard'
})
@Component({
  selector: 'page-onboard',
  templateUrl: 'onboard.html',
})
export class OnboardPage {

  @ViewChild(Slides) slider: Slides;

  slides = [
    {
      description: 'Lorem ipsum dolor sit amet',
      image: 'assets/img/logo.png',
      button: 'Próximo',
      anim: ''
    },
    {
      description: 'Lorem ipsum dolor sit amet',
      image: 'assets/img/logo.png',
      button: 'Próximo',
      anim: ''
    },
    {
      description: 'Lorem ipsum dolor sit amet',
      image: 'assets/img/logo.png',
      button: 'Próximo',
      anim: ''
    }
  ];


  constructor(public onboardingProvider: OnboardingProvider,
              public navCtrl: NavController) { }

  proximo() {
    if (this.slider.isEnd()) return this.navCtrl.push('login');
    this.slider.slideNext();
  }

  onboardingFinalizado() {
    this.onboardingProvider.visualizado(true)
      .subscribe(() => this.navCtrl.setRoot('acessar'))
  }

}
