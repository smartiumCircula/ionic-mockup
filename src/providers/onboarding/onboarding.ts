import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import 'rxjs/add/observable/fromPromise';
import { Observable } from 'rxjs/Observable';

/**
 * O OnboardingProvider abstrai o estado do onboarding, se o usuário já viu ou não e se deve gravar que viu.
 */
@Injectable()
export class OnboardingProvider {
  constructor(public storage: Storage) { }

  visualizado(viu?: true): Observable<boolean> {
    if (viu === undefined) return Observable.fromPromise(this.storage.get('onboard:visualizado'));
    return Observable.fromPromise(this.storage.set('onboard:visualizado', viu));
  }
}
